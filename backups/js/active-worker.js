jQuery(document).ready(function(){
	var timer = parseInt(iul.actions.timer)*1000,
    is_active = jQuery(document).data('is_active'),
    activeCount = jQuery(document).data('active_count'),
    idleTimer = null;

    jQuery(document).idleTimer(timer);

    jQuery('body').on('mousemove keydown scroll click mouseenter', function () {
        jQuery(document).data('is_active',true);
        is_active = true;
    });
        
    setInterval(function () {
        if(is_active && !activeCount){
            idle_user_status_call('active');
            jQuery(document).data('active_count',1);
            activeCount = 1;
        }
        
    }, (timer/2) );

    jQuery(document).bind("active.idleTimer", function(){
        idle_user_status_call('active');
    });

});

function idle_user_status_call(type){
    jQuery.ajax({
        type: 'POST',
        url: iul.ajaxurl,
        data: {action: 'update_user_time', callType:type},
        error: function(MLHttpRequest, textStatus, errorThrown){ console.log(errorThrown); },
        success: function(response){
            console.log(response);
        }
    });
}
